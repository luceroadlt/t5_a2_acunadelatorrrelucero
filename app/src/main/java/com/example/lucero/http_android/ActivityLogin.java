package com.example.lucero.http_android;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class ActivityLogin extends AppCompatActivity {

    private Button btnLoginAcceder;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        btnLoginAcceder = (Button) findViewById(R.id.btnLoginAcceder);

        btnLoginAcceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), ActivityMenu.class);
                startActivity(i);
            }
        });

    }




}



