package com.example.lucero.http_android;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import controlador.AnalizadorJSON;

/**
 * Created by Lucero on 05/12/2017.
 */

public class ActivityConsultas extends Activity {

    private EditText cajaConsultasNoControl,cajaConsultasNombre, cajaConsultasPA;
    public ListView listViewConsultar;

    ArrayList<String> listaDatos = new ArrayList<String>();


    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultas);

        cajaConsultasNombre = (EditText) findViewById(R.id.cajaConsultasNombre);
        listViewConsultar = (ListView) findViewById(R.id.listViewConsultar);

        new ConsultarAlumnos().execute();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, listaDatos);

        listViewConsultar.setAdapter(adapter);

//        cajaConsultasNombre.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//                cajaConsultasNombre.getText();
//
//            }
//        });


    }


    class ConsultarAlumnos extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... strings) {

            AnalizadorJSON analizadorJSON = new AnalizadorJSON();

            String url = "http://192.168.1.66/PruebasPHP/HTTP_Android/consulta_alumnos.php";

            JSONObject jsonObject = analizadorJSON.peticionHTTP(url);

            try {

                JSONArray jsonArray = jsonObject.getJSONArray("alumnos");

                for (int i=0; i<jsonArray.length(); i++){
                    String datos = jsonArray.getJSONObject(i).getString("nc")+ " - " +
                            jsonArray.getJSONObject(i).getString("n") + " - " +
                            jsonArray.getJSONObject(i).getString("pa") + " - " +
                            jsonArray.getJSONObject(i).getString("sa") + " - " +
                            jsonArray.getJSONObject(i).getString("e") + " - " +
                            jsonArray.getJSONObject(i).getString("s") + " - " +
                            jsonArray.getJSONObject(i).getString("c") ;

                    listaDatos.add(datos);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }
    }
}
