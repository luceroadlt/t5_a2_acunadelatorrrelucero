package com.example.lucero.http_android;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import controlador.AnalizadorJSON;

/**
 * Created by Lucero on 05/12/2017.
 */

public class ActivityAltas extends Activity {

    EditText cajaAltasNoControl, cajaAltasNombre, cajaAltasPA, cajaAltasSA, cajaAltasEdad, cajaAltasSemestre, cajaAltasCarrera;

    public static String mensaje;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_altas);

        cajaAltasNoControl = (EditText) findViewById(R.id.cajaAltasNoControl);
        cajaAltasNombre = (EditText) findViewById(R.id.cajaAltasNombre);
        cajaAltasPA = (EditText) findViewById(R.id.cajaAltasPA);
        cajaAltasSA = (EditText) findViewById(R.id.cajaAltasSA);
        cajaAltasEdad = (EditText) findViewById(R.id.cajaAltasEdad);
        cajaAltasSemestre = (EditText) findViewById(R.id.cajaAltasSemestre);
        cajaAltasCarrera = (EditText) findViewById(R.id.cajaAltasCarrera);
    }
    public void limpiarFormulario(View v){
        cajaAltasNoControl.setText("");
        cajaAltasNombre.setText("");
        cajaAltasPA.setText("");
        cajaAltasSA.setText("");
        cajaAltasEdad.setText("");
        cajaAltasSemestre.setText("");
        cajaAltasCarrera.setText("");

    }

    public void  agregarRegistro(View v){

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();

        Log.w("MSJ wifi", ni.isConnected() + "");

        if (ni!=null && ni.isConnected()){

            String nc = cajaAltasNoControl.getText().toString();
            String n = cajaAltasNombre.getText().toString();
            String ap = cajaAltasPA.getText().toString();
            String am = cajaAltasSA.getText().toString();
            String e = cajaAltasEdad.getText().toString();
            String s = cajaAltasSemestre.getText().toString();
            String c = cajaAltasCarrera.getText().toString();

            //usar clase interna para que realice la incersion
            new AgregarAlumno().execute(nc, n, ap, am, e, s , c);

            Toast.makeText(this, "Agregado!", Toast.LENGTH_SHORT).show();

        }//if
    }

    class AgregarAlumno extends AsyncTask<String, String, String> {

        @Override             //varArgs
        protected String doInBackground(String... datos) {

            Map<String, String> registros = new HashMap<>();

            registros.put( "nc",datos[0]);
            registros.put( "n",datos[1]);
            registros.put( "ap",datos[2]);
            registros.put( "am",datos[3]);
            registros.put( "e",datos[4]);
            registros.put( "s",datos[5]);
            registros.put( "c",datos[6]);

            Log.i("MSJ datos", Arrays.toString(datos));

            AnalizadorJSON aJSON = new AnalizadorJSON();

            //Emulador Genymotion
         //   String url = "http://10.0.3.2/PruebasPHP/HTTP_Android/altas_alumnos.php";

            //red local
            String url = "http://192.168.1.66/PruebasPHP/HTTP_Android/altas_alumnos.php";

            //emulador server
        //    String url = "http://176.48.16.5/HTTP_Android/altas_alumnos.php";

            String metodoEnvio = "POST";

            final JSONObject jsonObject = aJSON.peticionHTTP(url,metodoEnvio, registros);

            Log.i("MSJ JSON: ", jsonObject.toString());

            //enviar mnsj
            try{
                Log.i("MSJ", jsonObject.toString());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), jsonObject.toString(), Toast.LENGTH_LONG).show();
                    }
                });
                int exito = jsonObject.getInt("Exito");
                if (exito == 1){
                 //   Toast.makeText(getApplicationContext(), "Registro AGREGADO", Toast.LENGTH_LONG).show();
                    Log.i("MSJ", "REGISTRO AGREGADO");
                }else {
              //     Toast.makeText(getApplicationContext(), "ERROR EN AGREGACION", Toast.LENGTH_LONG).show();
                    Log.i("MSJ", "ERROR EN AGREGACION");
                }
            }catch(JSONException e){
                e.printStackTrace();
            }
            return null;
        }


    }

}//metodo agregar registro

