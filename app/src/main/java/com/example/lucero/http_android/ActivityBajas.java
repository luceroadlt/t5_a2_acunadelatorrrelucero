package com.example.lucero.http_android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import controlador.AnalizadorJSON;

/**
 * Created by Lucero on 05/12/2017.
 */

public class ActivityBajas extends AppCompatActivity {

    EditText cajaBajasNoControl;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bajas);
    }

    public void  eliminarRegistro(View v){

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();

        Log.w("MSJ wifi", ni.isConnected() + "");

        if (ni!=null && ni.isConnected()){

            String nc = cajaBajasNoControl.getText().toString();

         //   new ActivityBajas().EliminarAlumno.execute(nc);

            Toast.makeText(this, "Eliminado!", Toast.LENGTH_SHORT).show();

        }//if
    }

    class EliminarAlumno extends AsyncTask<String, String, String> {

        @Override             //varArgs
        protected String doInBackground(String... datos) {

            Map<String, String> registros = new HashMap<>();

            registros.get("nc");


            Log.i("MSJ datos", Arrays.toString(datos));

            AnalizadorJSON aJSON = new AnalizadorJSON();


            //red local
            String url = "http://192.168.1.66/PruebasPHP/HTTP_Android/bajas_alumnos.php";

            String metodoEnvio = "POST";

            final JSONObject jsonObject = aJSON.peticionHTTP(url,metodoEnvio, registros);

            Log.i("MSJ JSON: ", jsonObject.toString());

            //enviar mnsj
            try{
                Log.i("MSJ", jsonObject.toString());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), jsonObject.toString(), Toast.LENGTH_LONG).show();
                    }
                });
                int exito = jsonObject.getInt("Exito");
                if (exito == 1){
                    //   Toast.makeText(getApplicationContext(), "Registro AGREGADO", Toast.LENGTH_LONG).show();
                    Log.i("MSJ", "REGISTRO ELIMINADO");
                }else {
                    //     Toast.makeText(getApplicationContext(), "ERROR EN AGREGACION", Toast.LENGTH_LONG).show();
                    Log.i("MSJ", "ERROR EN ELIMINACION");
                }
            }catch(JSONException e){
                e.printStackTrace();
            }
            return null;
        }


    }
}
