package com.example.lucero.http_android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.view.View;

/**
 * Created by Lucero on 05/12/2017.
 */

public class ActivityMenu extends AppCompatActivity{

    Button btnMenuAltas, btnMenuBajas, btnMenuCambios, btnMenuConsultas;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        btnMenuAltas = (Button) findViewById(R.id.btnMenuAltas);
        btnMenuBajas = (Button) findViewById(R.id.btnMenuBajas);
        btnMenuCambios = (Button) findViewById(R.id.btnMenuCambios);
        btnMenuConsultas = (Button) findViewById(R.id.btnMenuConsultas);

//        btnMenuAltas.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(v.getContext(), ActivityAltas.class);
//            }
//        });
    }

    public void abrirrActivitys(View v) {



        Intent i;

        int CLAVE = 0;

        switch (v.getId()) {
            case R.id.btnMenuAltas:
                i = new Intent(this, ActivityAltas.class);
                startActivity(i);
                CLAVE = 1;
                startActivityForResult(i, CLAVE);
                break;

            case R.id.btnMenuBajas:
                i = new Intent(this, ActivityBajas.class);
                //   startActivity(i);
                CLAVE = 2;
                startActivityForResult(i, CLAVE);
                break;

            case R.id.btnMenuCambios:
                i = new Intent(this, ActivityCambios.class);

                // startActivity(i);
                CLAVE = 3;
                startActivityForResult(i, CLAVE);
                break;

            case R.id.btnMenuConsultas:
                i = new Intent(this, ActivityConsultas.class);
                // startActivity(i);
                CLAVE = 4;
                startActivityForResult(i, CLAVE);
                break;
        }

    }
}
